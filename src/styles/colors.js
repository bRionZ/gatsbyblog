module.exports = {
  background: "#fffff",
  white: "#ffffff",
  accent: "#3fc699",
  bright: "#ffffff",
  dark: "#333333",
  gray: "#555555",
  lightGray: "#bbbbbb",
  superLightGray: "#dedede"
};
