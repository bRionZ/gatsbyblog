const colors = require("../../src/styles/colors");

module.exports = {
  siteTitle: "Oka bRionZ - a starter blog for GatsbyJS", // <title>
  shortSiteTitle: "Oka bRionZ GatsbyJS Starter", // <title> ending for posts and pages
  siteDescription: "Oka bRionZ is a GatsbyJS starter.",
  siteUrl: "https://blog.okabrionz.com",
  pathPrefix: "",
  siteImage: "preview.jpg",
  siteLanguage: "en",
  // author
  authorName: "Oka bRionZ",
  authorTwitterAccount: "antihacx",
  // info
  infoTitle: "bRionZ Blog",
  infoTitleNote: "Personal blog",
  // manifest.json
  manifestName: "Oka bRionZ - a starter blog by GatsbyJS",
  manifestShortName: "OkabRionZ", // max 12 characters
  manifestStartUrl: "/",
  manifestBackgroundColor: colors.background,
  manifestThemeColor: colors.background,
  manifestDisplay: "standalone",
  // contact
  contactEmail: "me@okabrionz.com",
  // social
  authorSocialLinks: [
    { name: "github", url: "https://github.com/okabrionz" },
    { name: "twitter", url: "https://twitter.com/okabrionz" },
    { name: "facebook", url: "http://facebook.com/okabrionz" }
  ]
};
